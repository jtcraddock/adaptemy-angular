/*jslint indent: 2, maxerr: 50, white: true, browser: true, devel: true, vars: true */
'use strict';

angular.module('myApp.dashboard', ['ngRoute'])

.config(['$routeProvider', function($routeProvider) {
  $routeProvider.when('/dashboard', {
    templateUrl: 'views/dashboard/dashboard.html',
    controller: 'DashboardCtrl'
  });
}])
.controller('DashboardCtrl', ['$scope', '$rootScope', '$location', function($scope, $rootScope, $location) {
  console.log($rootScope.userType + typeof $rootScope.userType);
  if(!$rootScope.loggedIn){
    $location.path('login');
  }
}]);