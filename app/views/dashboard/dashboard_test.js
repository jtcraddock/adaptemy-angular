/*jslint indent: 4, maxerr: 50, white: true, browser: true, devel: true, vars: true */
/*global inject, expect, it, describe, beforeEach*/
'use strict';

describe('myApp.dashboard module', function() {

  beforeEach(module('myApp.dashboard'));

  describe('dashboard controller', function(){

    it('should ....', inject(function($controller) {
      //spec body
      var DashboardCtrl = $controller('DashboardCtrl');
      expect(DashboardCtrl).toBeDefined();
    }));

  });
});