/*jslint indent: 4, maxerr: 50, white: true, browser: true, devel: true, vars: true */
/*global inject, expect, it, describe, beforeEach*/

'use strict';

describe('myApp.login module', function() {

  beforeEach(module('myApp.login'));

  describe('login controller', function(){

    it('should ....', inject(function($controller) {
      //spec body
      var loginCtrl = $controller('LoginCtrl');
      expect(loginCtrl).toBeDefined();
    }));

  });
});