/*jslint indent: 2, maxerr: 50, white: true, browser: true, devel: true, vars: true */
'use strict';

angular.module('myApp.login', ['ngRoute'])

.config(['$routeProvider', function($routeProvider) {
  $routeProvider.when('/login', {
  templateUrl: 'views/login/login.html',
  controller: 'LoginCtrl'
  });
}])

.controller('LoginCtrl', ['$scope', '$location', '$rootScope', '$cookies', 'LoginRequest', function($scope, $location, $rootScope, $cookies, LoginRequest) {
  $scope.username = 'testJC';
  $scope.password = 'abcABC#123';

  $scope.login = function(){
    var payload = 'username='+$scope.username+'&password='+$scope.password,
        loginSuccessHandler,
        loginFaulureHandler;
    
    $scope.failedLogin = false;
    $scope.attemptingLogin = true;

    loginSuccessHandler = function(response){
      if (!response.token){
        //data doesn't match expected format
        loginFaulureHandler(response);
        return;
      }
      $scope.attemptingLogin = false;
      $rootScope.name = $scope.username;
      $rootScope.token = response.token;
      $rootScope.userType = response.type;
      $rootScope.loggedIn = true;
      if ($scope.rememberMe){
        $cookies.token = $rootScope.token;
        $cookies.name = $rootScope.name;
        $cookies.userType = $rootScope.userType;  
      }      
      $location.path('dashboard');     
    };

    loginFaulureHandler = function(){
      $scope.attemptingLogin = false;
      $scope.failedLogin = true;
    }; 

    LoginRequest.requestLogin(payload, loginSuccessHandler, loginFaulureHandler);
  };
}]);