/*jslint indent: 2, maxerr: 50, white: true, browser: true, devel: true, vars: true */
'use strict';

angular.module('myApp.signup', ['ngRoute'])

.config(['$routeProvider', function($routeProvider) {
  $routeProvider.when('/signup', {
    templateUrl: 'views/signup/signup.html',
    controller: 'SignUpCtrl'
  });
}])
.controller('SignUpCtrl', ['$scope', '$location', '$rootScope', '$cookies', 'APIRequest', 'LoginRequest', function($scope, $location, $rootScope, $cookies, APIRequest, LoginRequest) {
  $scope.userTypes = ['teacher', 'student'];
  $scope.selectedType = $scope.userTypes[0];
  $scope.signup = function(){
    var payload = 'username='+$scope.username+'&password='+$scope.password,
        signUpSuccessHandler,
        signUpFaulureHandler,
        loginSuccessHandler,
        loginFaulureHandler;

    signUpSuccessHandler = function(){
      var loginPayload = 'username='+$scope.username+'&password='+$scope.password;
      LoginRequest.requestLogin(loginPayload, loginSuccessHandler, loginFaulureHandler);
    };

    signUpFaulureHandler = function(){
      console.log("failure in signup");
    };

    loginSuccessHandler = function(response){
      if (!response.token){
        //data doesn't match expected format
        loginFaulureHandler(response);
        return;
      }
      $scope.attemptingLogin = false;
      $rootScope.name = $scope.username;
      $rootScope.token = response.token;
      $rootScope.userType = response.type;
      $rootScope.loggedIn = true;    
      $location.path('dashboard');     
    };

    loginFaulureHandler = function(){
      $scope.attemptingLogin = false;
      $scope.failedLogin = true;
    }; 

    

    if ($scope.selectedType === $scope.userTypes[0]){
      APIRequest.createTeacher(payload, signUpSuccessHandler, signUpFaulureHandler);
    }
    else if ($scope.selectedType === $scope.userTypes[1]){
      APIRequest.createStudent(payload, signUpSuccessHandler, signUpFaulureHandler);
    }
  };
}]);