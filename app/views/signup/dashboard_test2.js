/*jslint indent: 4, maxerr: 50, white: true, browser: true, devel: true, vars: true */
/*global inject, expect, it, describe, beforeEach*/
'use strict';

describe('myApp.dashboard module', function() {

  beforeEach(module('myApp.signup'));

  describe('signup controller', function(){

    it('should ....', inject(function($controller) {
      //spec body
      var DashboardCtrl = $controller('SignUpCtrl');
      expect(DashboardCtrl).toBeDefined();
    }));

  });
});