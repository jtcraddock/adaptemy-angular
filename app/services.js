/*jslint indent: 4, maxerr: 50, white: true, browser: true, devel: true, vars: true */
/*global $, jQuery, google*/

'use strict';

/* Services */

var adaptemyServices = angular.module('adaptemyServices', ['ngResource']),
	requestURL = 'json/',
  //loginRequestURL = 'json/login.json', requestMethod = 'GET',
  loginRequestURL = 'http://localhost/html/adaptemy2015/user/login', requestMethod = 'POST',
  APIRequestURL = 'http://localhost/html/adaptemy2015/user/', APIRequestMethod = 'POST';
  //APIRequestURL = 'json/', APIRequestMethod = 'GET';

adaptemyServices.factory('APIRequest', ['$resource',
  function($resource){
    return $resource(APIRequestURL+':requestDestination', {}, {
      createTeacher: {  method:requestMethod,
                        params:{requestDestination:'createteacher'},
                        headers: {"Content-Type": 'application/x-www-form-urlencoded'}
                      },
      createStudent: {  method:requestMethod,
                        params:{requestDestination:'createstudent'},
                        headers: {"Content-Type": 'application/x-www-form-urlencoded'}
                      }
    });
  }]);

adaptemyServices.factory('LoginRequest', ['$resource',
  function($resource){
    return $resource(loginRequestURL, {},{
      requestLogin: { method:requestMethod,
                      headers: {"Content-Type": 'application/x-www-form-urlencoded'}
                    }
	});
  }]);