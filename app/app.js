/*jslint indent: 4, maxerr: 50, white: true, browser: true, devel: true, vars: true */
'use strict';

// Declare app level module which depends on views, and components
angular.module('myApp', [
  'ngRoute',
  'adaptemyServices',
  'ngCookies',
  'myApp.login',
  'myApp.dashboard',
  'myApp.signup'
])

.config(['$routeProvider', function($routeProvider) {
  $routeProvider.otherwise({redirectTo: '/login'});
}])
.controller('MainCtrl', ['$rootScope', '$cookies', '$location', function($rootScope, $cookies, $location) {
  $rootScope.logout = function(){
    $rootScope.name = false;
    $rootScope.token = false;
    $rootScope.loggedIn = false;
    delete $cookies.token;
    delete $cookies.name;
    $location.path('login');  
  };
}])
.run(function($location, $cookies, $rootScope){
  $rootScope.loggedIn = false;
	//check for previous login
	if(!$cookies.token){
    //re-direct to login page if user is not signed in. Allow access to signup page.
    if ($location.path() !== '/signup'){
      console.log("re-directing to login from "+$location.path());
      $location.path('login');  
    }
	}
  else{
    //TODO: verify login again
    $rootScope.loggedIn = true;
    $rootScope.name = $cookies.name;
    $rootScope.token = $cookies.token;
    $rootScope.userType = $cookies.userType;
  }
});
