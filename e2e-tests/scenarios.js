'use strict';

/* https://github.com/angular/protractor/blob/master/docs/toc.md */

describe('my app', function() {

  browser.get('index.html');

  it('should automatically redirect to /login when location hash/fragment is empty', function() {
    expect(browser.getLocationAbsUrl()).toMatch("/login");
  });


  describe('login', function() {

    beforeEach(function() {
      browser.get('index.html#/login');
    });


    it('should render login view when user navigates to /login', function() {
      expect(element.all(by.css('.form-group label')).first().getText()).
        toMatch("Username");
    });

    it('should not validate and display error message when user enters invalid login info', function(){
      var user = element(by.model('username')),
          pass = element(by.model('password')),
          submit = element.all(by.css('.form-actions button'));
      
      user.clear().sendKeys('acb');
      pass.clear().sendKeys('123456aaa');
      submit.click();
      expect(browser.getLocationAbsUrl()).toMatch("/login");
      expect($('#failed-login-message').isDisplayed()).toBeTruthy();
    });
    it('should validate and display dashboard view when user enters valid login info', function(){
      var user = element(by.model('username')),
          pass = element(by.model('password')),
          submit = element.all(by.css('.form-actions button'));
      
      user.clear().sendKeys('john123456');
      pass.clear().sendKeys('abcABC123#123');
      submit.click();
      expect(browser.getLocationAbsUrl()).toMatch("/dashboard");
      var username = element(by.id('username'));
      expect(username.getText()).toBe("john123456");
    });

  });

});
