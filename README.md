View [angular seed project](https://github.com/angular/angular-seed) for instillation instructions.

Deployment consists of copying the 'app' folder to the production server.

View [Angular Phone Catalog Tutorial](https://docs.angularjs.org/tutorial) for a good introduction to the development practices.